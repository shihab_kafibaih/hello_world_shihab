import 'package:flutter/material.dart';
import './product.dart';

class ProductManager extends StatefulWidget {
  final String startingProduct;
  ProductManager(this.startingProduct);

  @override 
  _ProductManagerState createState() {
     print('[ProductManagerState] createState()');
     return _ProductManagerState();
    }
}

class _ProductManagerState extends State<ProductManager> {
  List<String> _products = [];

  @override
  void initState() {
    print('[ProductManagerState] initState()');
    _products.add(widget.startingProduct);
    super.initState();
  }

@override
  void didUpdateWidget(ProductManager oldWidget) {
    print('[ProductManager State] didUpdateWidget()');
    super.didUpdateWidget(oldWidget);
  }
  @override
  Widget build(BuildContext context) {
    print('[ProductManagerState] build()');
    return Column(
      children: <Widget>[
        Container(
      margin: EdgeInsets.only( top: 10.0),
      child: RaisedButton(
        color: Theme.of(context).primaryColor,
      child : Text('Add Product'),
      onPressed: (){
        setState(() {
          _products.add('FOOD TESTER');
          print(_products);
        });
      },
      ),
    ),
    Products(_products),
      ],
    
    );
  }
}

