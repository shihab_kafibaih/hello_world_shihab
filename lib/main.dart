import 'package:flutter/material.dart';
import "./product_manager.dart";

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.deepOrange
      ),
      title: "SHIHAB APP",
      home: Scaffold(
        appBar: AppBar(
          title: Text("BAKERY"),
        ) ,
        body: ProductManager('Food Tester'),
        ),
    );
  }
}